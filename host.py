from pyzabbix.api import ZabbixAPI
from zabbix import solicitud
from statistics import mean
from wisphub import clientes_nodo

class Item():
    def __init__(self,itemid,name,netcom:bool=True) -> None:
        """Inicializando clase"""
        self.itemid = itemid
        self.name = name
        self.muestras = []
        self.netcom = netcom
        self.clientes = None

    def act_est(self):
        """Actualiza las estadísticas de la interfaz de acuerdo a las muestras obtenidas (máximo, promedio y cantidad de muestras)."""
        self.max = max(self.muestras)
        self.ave = mean(self.muestras)
        self.cantidad_muestras = len(self.muestras)
    
    def prom_clientes(self):
        """Calcula el tráfico máximo y promedio por cliente según el número de clientes en el nodo.
        Si por alguna razón no hay clientes (o no logra conectar con la API de Wisphub), la respuesta será None."""
        try:
            m = float('{:.2f}'.format(self.max / self.clientes))
            n = float('{:.2f}'.format(self.ave / self.clientes))
        except ZeroDivisionError:
            m = None
            n = None
        
        self.max_cliente = m
        self.ave_cliente = n

class Host():
    def __init__(self,hostid,dict_interfaces,wisphub,olt:bool=False):
        """Inicializando clase."""
        self.itemid = hostid
        self.interfaces = []
        self.olt = olt
        self.wisphub = wisphub
        
        for i in dict_interfaces:
            self.interfaces.append(Item(dict_interfaces[i]['itemid'],dict_interfaces[i]['name'],dict_interfaces[i]['netcom']))
        if olt:
            self.interfaces.append(Item('0000','SUMA OLTS',self.interfaces[1].netcom))

    def muestrear(self,zapi:ZabbixAPI,desde,hasta):
        """La función toma las muestras desde la API de Zabbix para cada interfaz del Host dentro de un lapso de tiempo determinado"""
        if self.olt:
            for i in self.interfaces[:-1]:
                solicitud(zapi,i.itemid,i.muestras,desde,hasta,True,self.interfaces[-1].muestras)
                i.act_est()
                print(i.max)
            self.interfaces[-1].act_est()
        else:
            for i in self.interfaces:
                solicitud(zapi,i.itemid,i.muestras,desde,hasta)
                i.act_est()
    
    def get_clientes(self):
        """La función consulta a la API de Wisphub a través de clientes_nodo para obtener la cantidad de clientes en
        las diferente interfaces o, en caso de tratarse de varias OLTs, el total en el nodo"""
        if self.olt:
            self.interfaces[-1].clientes = clientes_nodo(self.wisphub,self.interfaces[-1].netcom)
            self.interfaces[-1].prom_clientes()
        else:
            for i in self.interfaces:
                i.clientes = clientes_nodo(self.wisphub,i.netcom)
                i.prom_clientes()
    
    def get_clientes_dic(self,dic:dict):
        """[EN PROCESO] Se busca hacer una sola consulta a la API de Wisphub para reducir el tiempo de corrida"""
        if self.olt:
            if self.interfaces[-1].netcom:
                l = dic['netcom']