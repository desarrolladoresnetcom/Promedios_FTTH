from pyzabbix.api import ZabbixAPI
import datetime
from io import open
from host import Host

#INICIO DE SESIÓN PARA ZABBIX
zapi = ZabbixAPI(url="http://zabbix.netcomplusve.com/zabbix",
                 user="monitoreo_api",
                 password="oH!JyQeCmtRSv3tQ")

#INICIANDO DICCIONARIO CON LA INFORMACIÓN DE LOS HOSTS E INTERFACES A MONITOREAR [IDEALMENTE, PASAR A LEER DE UN TXT]
dict_host = {
'MK-NETCOM-CE-CARIBE-RO-01':Host('10573',{
		'A':{'itemid': '117324', 'name': 'Interface VLAN-200-OLT-01(): Bits sent', 'netcom': False},
		'B':{'itemid': '117329', 'name': 'Interface VLAN-200-OLT-02(): Bits sent', 'netcom': False},
		'C':{'itemid': '117330', 'name': 'Interface VLAN-200-OLT-03(): Bits sent', 'netcom': False},
		'D':{'itemid': '117331', 'name': 'Interface VLAN-200-OLT-04(): Bits sent', 'netcom': False}
		},'CARIBE',True),

'MK-NETCOM-DAYCO-RO-ACCESO': Host('10555',{
		'A':{'itemid': '99319', 'name': 'Interface VLAN-400-FTTH-TECNE(Parque Valencia, Quizanda, Isabelica): Bits sent', 'netcom': False},
		'B':{'itemid': '99320', 'name': 'Interface VLAN-401-FTTH-OLT-03-NETCOM(Netcom Zona Sur): Bits sent', 'netcom': True}
		},'PASEO LAS INDUSTRIAS'),

'MK-NETCOM-FUNDACION-MENDOZA-RO-01': Host('10560',{
		'A': {'itemid': '59685', 'name': 'Interface VLAN-200-FTTH-OLT-01(): Bits sent', 'netcom': True},
		'B': {'itemid': '74524', 'name': 'Interface VLAN-200-FTTH-OLT-02(): Bits sent', 'netcom': True}
		},'FUNDACION MENDOZA',True),

'MK-NETCOM-GUACARA-RO-01': Host('10568',{
		'A': {'itemid': '111955', 'name': 'Interface VLAN-200-FTTH(): Bits sent', 'netcom': False}
		},'GUACARA'),

'MK-NETCOM-ISLA-LARGA-RO-01': Host('10542',{
		'A': {'itemid': '50489', 'name': 'Interface VLAN-200-CLIENTES-FTTH(): Bits sent', 'netcom': True},
		},'ISLA LARGA'),

'MK-NETCOM-ISLA-LARGA-RO-01 (LA SORPRESA)': Host('10542',{
		'A':{'itemid': '117745', 'name': 'Interface VLAN-220-FTTH-LA-SORPRESA(): Bits sent', 'netcom': True}
		},'LA SORPRESA'),

'MK-NETCOM-LOS-PARQUES-RO-01': Host('10540',{
		'A':{'itemid': '118982', 'name': 'Interface VLAN-200-FTTH-NETCOM(): Bits sent', 'netcom': True},
		'B':{'itemid': '118983', 'name': 'Interface VLAN-201-FTTH-TECNE(): Bits sent', 'netcom': False}
		},'LOS PARQUES'),

'MK-NETCOM-MIRADOR-RO-01': Host('10549',{
		'A':{'itemid': '86950', 'name': 'Interface VLAN-200-FTTH-NETCOM(): Bits sent', 'netcom': True}
		},'MIRADOR'),

'MK-NETCOM-SHANGRI-LA-RO-01': Host('10558',{
		'A':{'itemid': '54333', 'name': 'Interface VLAN-200-FTTH-SHANGRI-LA(): Bits sent', 'netcom': True}
		},'SHANGRI-LA'),

'MK-NETCOM-TORRE-EJECUTIVA-RO-01': Host('10541',{
		'A':{'itemid': '49625', 'name': 'Interface VLAN-200-CLIENTES-FTTH(): Bits sent', 'netcom': True}
	},'TORRE EJECUTIVA')
}
#***********************************HASTA AQUÍ INFO DE HOSTS***********************************************
while True:
	if datetime.datetime.now().strftime('%d/%m/%Y %H:%M:%S') == f"{datetime.datetime.now().strftime('%d/%m/%Y')} 00:00:00":
#VARIABLES DE TIEMPO EN EL QUE SE REALIZARÁ EL MONITOREO
		now = datetime.datetime.today() - datetime.timedelta(days=1)
		desde = int(datetime.datetime(now.year,now.month,now.day,21,0,0).timestamp())
		hasta = int(datetime.datetime(now.year,now.month,now.day,23,30,0).timestamp())

#MENSAJE DE INICIO DEL SCRIPT
		print(f'Monitoreo desde: {datetime.datetime.fromtimestamp(desde)}. Hasta {datetime.datetime.fromtimestamp(hasta)}')

#CONSULTAS A APIS DE ZABBIX Y WISPHUB PARA CADA ELEMENTO DEL DICCIONARIO DE HOSTS
		for i in dict_host.keys():
			dict_host[i].muestrear(zapi,desde,hasta)
			dict_host[i].get_clientes()

#IMPRESIÓN DE DATOS Y FORMATO
		with open('trafico_api.txt','a+',encoding='utf-8') as f:
			f.write(f"*** \t Monitoreo {datetime.datetime.fromtimestamp(desde)} - {datetime.datetime.fromtimestamp(hasta)} \t ***\n")        
			for i in dict_host:
				f.write(f"{i}:\n")
				for v in dict_host[i].interfaces:
					f.write(f"\t{v.name}:\n\t\tMáximo: {float('{:.2f}'.format(v.max))}[Mbps]. Promedio: {float('{:.2f}'.format(v.ave))}[Mbps]\n")
				f.write('\n')

		with open('promedios_FTTH.txt','a+',encoding='utf-8') as f:
			f.write(f"*** \t Promedios {datetime.datetime.fromtimestamp(desde)} - {datetime.datetime.fromtimestamp(hasta)} \t ***\n")
			for i in dict_host:
				f.write(f"{dict_host[i].wisphub}:\n")
				if dict_host[i].olt:
					f.write(f"\t{dict_host[i].interfaces[-1].name}\n\t Clientes: {dict_host[i].interfaces[-1].clientes}. Máx: {dict_host[i].interfaces[-1].max_cliente} [Mbps/clientes]. Prom: {dict_host[i].interfaces[-1].ave_cliente} [Mbps/clientes]\n")
				else:
					for v in dict_host[i].interfaces:
						if v.netcom:
							f.write('\tNETCOM:\n')
						else:
							f.write('\tTECNE:\n')
						f.write(f"\t{v.name}\n\t Clientes: {v.clientes}. Máx: {v.max_cliente} [Mbps/clientes]. Prom: {v.ave_cliente} [Mbps/clientes]\n")
			f.write('\n')