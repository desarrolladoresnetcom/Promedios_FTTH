import requests
from datetime import datetime

def clientes_nodo(nombre_nodo, netcom:bool):
    """La función hace una solicitud al API de NETCOM o TECNE según el valor del argumento netcom (boolean). 
    Luego, de acuerdo al nombre del nodo, filtra los clientes con estado activo o gratis con algún plan de FTTH en dicho nodo, regresando
    la cantidad de clientes que cumplen con las condiciones"""
    if netcom:
        r = requests.api.get('https://api.wisphub.net/api/clientes/',headers={'Authorization':'Api-Key VICpPN03.Gdif5OLPdgbkp8sSIVWtDKLDQWG8RCn6'},params={'limit':'10000'})
    else :
        r = requests.api.get('https://api.wisphub.io/api/clientes/',headers={'Authorization':'Api-Key kLNBbHHU.ukE5xO5DBsGXfqg1ROZg8qzdiXfmpmZr'},params={'limit':'10000'})
    lista = r.json()['results']
    lista_filtrada = []
    for i in lista:
        if i['router']['nombre'] == nombre_nodo and 'FTTH' in i['plan_internet']['nombre'] and (i['estado'] == 'Activo' or i['estado'] == 'Gratis') and (datetime.strptime(i['fecha_instalacion'],'%m/%d/%Y %H:%M:%S')< datetime.today()):
            lista_filtrada.append(i)
    return len(lista_filtrada)

def clientes_full():
    """Regresa un diccionario con las listas completas de clientes en NETCOM y TECNE (EN PROGRESO)"""
    netcom = requests.api.get('https://api.wisphub.net/api/clientes/',headers={'Authorization':'Api-Key VICpPN03.Gdif5OLPdgbkp8sSIVWtDKLDQWG8RCn6'},params={'limit':'10000'}).json()['results']
    tecne = requests.api.get('https://api.wisphub.io/api/clientes/',headers={'Authorization':'Api-Key kLNBbHHU.ukE5xO5DBsGXfqg1ROZg8qzdiXfmpmZr'},params={'limit':'10000'}).json()['results']
    return {'netcom':netcom,'tecne':tecne}

