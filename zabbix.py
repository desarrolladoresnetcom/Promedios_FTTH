from pyzabbix.api import ZabbixAPI
from statistics import mean

def solicitud(zapi: ZabbixAPI,itemids,L:list,desde:int,hasta:int,olt:bool=False,sumas_olt:list=None):
    values = zapi.do_request('history.get',
                           {'itemids':[itemids],
                            'sortfield': 'clock',
                            'sortorder': 'DESC',
                            'time_from':desde,
                            'time_till':hasta,
                             })
    if values['result']:
        if olt:
            if len(sumas_olt) != len(values['result']):
                while len(sumas_olt) < len(values['result']):
                    sumas_olt.append(0)
    if values['result']:
        for i,v in enumerate(values['result']):
            L.append(float("{:.2f}".format(int(v['value'])/1000000)))
            if olt:
                sumas_olt[i] += float("{:.2f}".format(int(v['value'])/1000000))
    else:
        L.append(0)